# 暂停更新，不要下载！
正在开发基于.NET Core 3.0版本。发布时间未知，敬请耐心等待 :tw-1f60a: 

# 关于金汇进销存
金汇进销存是一款免费开源的商品销售管理软件。  
包含商品管理、采购、销售、库存、客户、报表等模块。  
软件功能完善，界面简洁，上手容易，操作方便。  
适合各类中小商店超市使用。  
金汇进销存免费下载安装使用，没有功能时间限制。  

# 界面展示
![默认主页](https://images.gitee.com/uploads/images/2018/0809/174306_c6caeabd_801294.jpeg "JinHuiJXC10.jpg")
![商品管理](https://images.gitee.com/uploads/images/2018/0809/174353_00870161_801294.jpeg "JinHuiJXC11.jpg")
![销售管理](https://images.gitee.com/uploads/images/2018/0809/174414_654af0ff_801294.jpeg "JinHuiJXC12.jpg")
![客户管理](https://images.gitee.com/uploads/images/2018/0809/174453_5b8df707_801294.jpeg "JinHuiJXC13.jpg")
![商品添加](https://images.gitee.com/uploads/images/2018/0809/174528_baed77ce_801294.jpeg "JinHuiJXC14.jpg")
![登录主页](https://images.gitee.com/uploads/images/2018/0809/174604_8dc4f0db_801294.jpeg "JinHuiJXC15.jpg")

# 版权声明
金汇进销存源码基于GPLv3协议开源。  
您可以免费下载安装使用。  
您可以添加、修改、扩展源码。但是必须保持免费开源，仍然保留金汇进销存软件名称。  
禁止修改删除版权声明。  

# 开发环境
开发架构：C# 6.0  
开发平台：.NET Framework 4.6  
开发工具：Visual Studio 2017  
前端框架：EasyUI、jQuery  
后端框架：ASP.NET Web API  
数据存储：SQL Server 2016  


# 运行环境
电脑硬件：2G以上内存、40G以上硬盘  
操作系统：Windows 7/8/10、Server 2013/6/8  
运行平台：IIS 7/8/10 、.NET Framework 4.6  
数据存储：SQL Server 2016/8 Express  

# 免责声明
金汇进销存及所附带的文件是作为不提供任何明确的或隐含的赔偿或担保的形式提供的。   
用户出于自愿而使用本软件，您必须了解使用本软件的风险。  
在尚未购买产品技术服务之前，我们不承诺提供任何形式的技术支持、使用担保，  
也不承担任何因使用本软件而产生问题的相关责任。

# 定制服务
如果您需要功能定制、接口适配、硬件搭配等服务，请将具体需求发送邮件至205265245#qq.com。   
我们会及时与您联系。
